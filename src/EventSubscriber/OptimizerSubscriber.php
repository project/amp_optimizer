<?php

namespace Drupal\amp_optimizer\EventSubscriber;

use AmpProject\Optimizer\ErrorCollection;
use AmpProject\Optimizer\TransformationEngine;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Optimizer event subscriber.
 *
 * @package Drupal\amp_optimizer\EventSubscriber
 */
class OptimizerSubscriber implements EventSubscriberInterface {

  /**
   * Configuration storage name.
   */
  public const CONFIG_NAME = 'amp_optimizer.settings';

  /**
   * The response transformation engine.
   *
   * @var \AmpProject\Optimizer\TransformationEngine
   */
  protected $transformationEngine;

  /**
   * The configuration.
   *
   * @var array|mixed
   */
  protected $config;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The error collection.
   *
   * @var \AmpProject\Optimizer\ErrorCollection
   */
  protected $errorCollection;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new \Drupal\amp_optimizer\EventSubscriber\OptimizerSubscriber object.
   *
   * @param \AmpProject\Optimizer\TransformationEngine $transformation_engine
   *   The transformation engine.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(TransformationEngine $transformation_engine, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_channel_factory, AccountProxyInterface $current_user) {
    $this->transformationEngine = $transformation_engine;
    $this->config = $config_factory->get(static::CONFIG_NAME)->get() ?? [];
    $this->logger = $logger_channel_factory->get('amp_optimizer');
    $this->errorCollection = new ErrorCollection();
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => [
        ['optimizer', -9999],
      ],
    ];
  }

  /**
   * Optimizer event subscriber callback.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent|\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   Event instance.
   */
  public function optimizer($event): void {
    if ($this->currentUser->isAuthenticated()) {
      return;
    }
    if (
      !array_key_exists('transform_enabled', $this->config) ||
      FALSE === (bool) $this->config['transform_enabled'] ||
      !$event->isMasterRequest() ||
      !$this->isAmpHtml($event->getResponse(), $event->getRequest())
    ) {
      return;
    }

    $optimized_html = $this->transformationEngine->optimizeHtml(
      $event->getResponse()->getContent(),
      $this->errorCollection
    );

    $this->handleErrors();

    $event->getResponse()->setContent($optimized_html);
  }

  /**
   * Checks if the response format is HTML.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return bool
   *   FALSE if the response is not an instance of HtmlResponse.
   */
  private function isAmpHtml(Response $response, Request $request): bool {
    if (!($response instanceof HtmlResponse) || $request->getRequestFormat() !== 'html') {
      return FALSE;
    }

    return preg_match(
      '/<html.*(\samp(>|\s.*>|=.*>)|\s⚡(>|\s.*>|=.*>))/muU',
      $response->getContent()
    );
  }

  /**
   * Error logger.
   */
  private function handleErrors(): void {
    if ($this->errorCollection->count() > 0) {
      foreach ($this->errorCollection as $error) {
        $this->logger->error(sprintf(
          "AMP-Optimizer Error code: %s\nError Message: %s\n",
          $error->getCode(),
          $error->getMessage()
        ));
      }
    }
  }

}
